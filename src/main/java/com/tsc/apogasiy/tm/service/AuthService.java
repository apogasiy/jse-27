package com.tsc.apogasiy.tm.service;

import com.tsc.apogasiy.tm.api.repository.IAuthRepository;
import com.tsc.apogasiy.tm.api.service.IAuthService;
import com.tsc.apogasiy.tm.api.service.IUserService;
import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import com.tsc.apogasiy.tm.exception.empty.EmptyLoginException;
import com.tsc.apogasiy.tm.exception.empty.EmptyPasswordException;
import com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import com.tsc.apogasiy.tm.model.User;
import com.tsc.apogasiy.tm.util.HashUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

@RequiredArgsConstructor
public class AuthService implements IAuthService {

    @Nullable
    private final IAuthRepository authRepository;

    @Nullable
    private final IUserService userService;

    @Override
    @NotNull
    public String getCurrentUserId() {
        @NotNull final String userId = authRepository.getCurrentUserId();
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return userId;
    }

    @Override
    public void setCurrentUserId(@Nullable final String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        @Nullable final String currentUserId = authRepository.getCurrentUserId();
        return (Optional.ofNullable(currentUserId).isPresent() || !currentUserId.isEmpty());
    }

    @Override
    public boolean isAdmin() {
        @Nullable final Role role = userService.findById(getCurrentUserId()).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent() || login.isEmpty())
            throw new EmptyLoginException();
        if (password == null || password.isEmpty())
            throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        if (user.getLocked())
            throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.encrypt(userService.getPropertyService(), password);
        if (!hash.equals(user.getHashedPassword()))
            throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth())
            throw new AccessDeniedException();
        setCurrentUserId(null);
    }

    @Override
    public void checkRoles(final Role... roles) {
        if (roles == null || roles.length == 0)
            return;
        @NotNull final User user = userService.findById(getCurrentUserId());
        if (user == null)
            throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null)
            throw new AccessDeniedException();
        for (@NotNull final Role item : roles) {
            if (item.equals(role))
                return;
        }
        throw new AccessDeniedException();
    }

}
