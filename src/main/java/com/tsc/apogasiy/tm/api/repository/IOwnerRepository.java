package com.tsc.apogasiy.tm.api.repository;

import com.tsc.apogasiy.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    @NotNull
    List<E> findAll(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator);

    @NotNull
    E add(@NotNull final String userId, @NotNull final E entity);

    @Nullable
    E findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    E findByIndex(@NotNull final String userId, @NotNull final Integer index);

    void clear(@NotNull final String userId);

    @Nullable
    E removeById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    E removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    void remove(@NotNull final String userId, @NotNull final E entity);

    @NotNull
    Integer getSize(@NotNull final String userId);

}
