package com.tsc.apogasiy.tm.exception.entity;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found!");
    }

}
