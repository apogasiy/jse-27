package com.tsc.apogasiy.tm.exception.entity;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User was not found!");
    }

}
