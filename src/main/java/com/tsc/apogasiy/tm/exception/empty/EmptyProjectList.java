package com.tsc.apogasiy.tm.exception.empty;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyProjectList extends AbstractException {

    public EmptyProjectList() {
        super("Error! Project list is empty!");
    }

}
