package com.tsc.apogasiy.tm.command.data;

import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @Override
    public @NotNull String getCommand() {
        return "data-load-bin";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Load state from binary";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(serviceLocator.getPropertyService().getDTOBinFileName());
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        setDomain((Domain) objectInputStream.readObject());
        objectInputStream.close();
        fileInputStream.close();
    }

}
