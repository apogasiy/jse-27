package com.tsc.apogasiy.tm.dto;

import com.tsc.apogasiy.tm.model.Project;
import com.tsc.apogasiy.tm.model.Task;
import com.tsc.apogasiy.tm.model.User;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private List<User> users;

}
